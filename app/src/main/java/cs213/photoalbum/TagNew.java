package cs213.photoalbum;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

/**
 * Created by admin on 12/14/16.
 */

public class TagNew extends AppCompatActivity {
    public static final String TAG_TYPE="tagtype";
    public static final String TAG_VALUE="tagvalue";
    public static final String TAG_INDEX="tagindex";
    public static final int ADD_TAG_CODE=1;
    private EditText tagtype, tagvalue;
    private int tagindex;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_new);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        //get the fields
        tagtype=(EditText)findViewById(R.id.new_tag_type);
        tagvalue=(EditText)findViewById(R.id.new_tag_value);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            tagindex=bundle.getInt(TAG_INDEX);
            tagtype.setText(bundle.getString(TAG_TYPE));
            tagvalue.setText(bundle.getString(TAG_VALUE));
        }

    }
    // called when the user taps the Back button
    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
    public void save(View view){
        //gather all data
        String type=tagtype.getText().toString();
        String value=tagvalue.getText().toString();
        //no typeless tag, only person and location are valid tag types

    }
}
