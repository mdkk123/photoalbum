package cs213.photoalbum;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by xliu189 on 11/20/2016.
 */
class Tag implements Serializable, Cloneable {
    String type;
    String value;

    public Tag(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void changeType(String newType) {
        this.type = newType;
    }

    public void changeValue(String newValue) {
        this.value = newValue;
    }

    public boolean equals(Tag otherTag) {
        if (this == otherTag) {
            return true;
        }
        return this.value.equals(otherTag.value)
                && this.type.equals(otherTag.type);
    }

    @Override
    public String toString() {
        return "{" + type + " : " + value + "}";
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

public class Tags extends AppCompatActivity {
    public static final int ADD_TAG_CODE=1;
    public static final int SHOW_TAG_CODE=2;
    public static final int REMOVE_TAG_CODE=3;
    private ListView listView;
    private ArrayList<Tag> tags;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        //
        listView=(ListView)findViewById(R.id.tag_list);
        listView.setAdapter(new ArrayAdapter<Tag>(this,R.layout.tag,tags));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>parent,View view, int position, long id){
                showTag(position);
            }
        });
    }

    public void showTag(int pos){
        Bundle bundle=new Bundle();
        Tag tag=tags.get(pos);
        bundle.putInt(TagDetail.TAG_INDEX,pos);
        bundle.putString(TagDetail.TAG_TYPE,tag.getType());
        bundle.putString(TagDetail.TAG_VALUE,tag.getValue());
        Intent intent=new Intent(this,TagDetail.class);
        startActivityForResult(intent,SHOW_TAG_CODE);
    }
    public void addTag(){

        Intent intent=new Intent(this, TagNew.class);
        startActivityForResult(intent,ADD_TAG_CODE);
    }
    public void deleteTag(int pos){
        Bundle bundle=new Bundle();
        Tag tag=tags.get(pos);
        bundle.putInt(TagDetail.TAG_INDEX,pos);
        bundle.putString(TagDetail.TAG_TYPE,tag.getType());
        bundle.putString(TagDetail.TAG_VALUE,tag.getValue());
        Intent intent=new Intent(this,TagDetail.class);
        startActivityForResult(intent,REMOVE_TAG_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent intent){
        if(resultCode!=RESULT_OK){
            return;
        }
        Bundle bundle = intent.getExtras();
        if(bundle==null){
            return;
        }

        String type=bundle.getString(TagDetail.TAG_TYPE);
        String value=bundle.getString(TagDetail.TAG_VALUE);
        int index=bundle.getInt(TagDetail.TAG_INDEX);
        if(requestCode==SHOW_TAG_CODE){

            Tag tag=tags.get(index);
            tag.type=type;
            tag.value=value;
        }
        else if(requestCode==REMOVE_TAG_CODE){
            tags.remove(index);
        }
        else if(requestCode==ADD_TAG_CODE){
            type=bundle.getString(TagNew.TAG_TYPE);
            value=bundle.getString(TagNew.TAG_VALUE);
            Tag tag=new Tag(type,value);
            tags.add(tag);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.photo_item_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_add_tag:
                addTag();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Tags Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override

    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
