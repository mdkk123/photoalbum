package cs213.photoalbum;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class models the album objects used across the application.
 * Contains abilities that are generalized for albums.
 *
 * @created 11/14/2016
 */
class Album implements Serializable, Cloneable {

    String name;
    Photo oldestPhoto;
    Photo newestPhoto;
    Photo currentPhoto;
    List<Photo> photoList;


    public Album(String name) {
        this.name = name;
        this.photoList = new ArrayList<Photo>();
        currentPhoto = null;
        oldestPhoto = null;
        newestPhoto = null;
    }

    /**
     * Getter for the list of photos in the current album.
     *
     * @return list of photos
     */
    public List<Photo> getPhotoList() {
        return photoList;
    }

    /**
     * Getter for the name of the album.
     *
     * @return album name.
     */
    public String getName() {
        return name;
    }

    /**
     * Iteration over existing photos to search for given Photo instance.
     * Note the comparison for Photo equality is based on path.
     *
     * @param photo Photo Object to be checked
     * @return boolean value. True if album contains photo.
     */
    public boolean containsPhoto(Photo photo) {
        for (Photo currPhoto : photoList) {
            if (photo.equals(currPhoto)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Getter for the current photo set.
     *
     * @return current photo.
     */
    public Photo getCurrentPhoto() {
        return currentPhoto;
    }

    /**
     * Setter for the current photo.
     *
     * @param currPhoto The photo to set the current photo to
     */
    public void setCurrentPhoto(Photo currPhoto) {
        currentPhoto = currPhoto;
    }

    /**
     * Given a photo, performs logic prior to adding it as part of the current album.
     *
     * @param path    File Object which contains the path.
     * @param caption Caption of the photo.
     * @return Reference of photo that was just added.
     */
    public Photo addPhoto(File path, String caption) {
        Photo newPhoto = new Photo(path, caption);
        if (containsPhoto(newPhoto)) {
            return null;
        }
        if (oldestPhoto == null || newestPhoto == null) {
            oldestPhoto = newPhoto;
            newestPhoto = newPhoto;
            photoList.add(newPhoto);
            return newPhoto;
        }
        if (newPhoto.compareTo(oldestPhoto) <= 0) {
            oldestPhoto = newPhoto;
        }
        if (newPhoto.compareTo(newestPhoto) >= 0) {
            newestPhoto = newPhoto;
        }
        photoList.add((newPhoto));
        return newPhoto;
    }

    /**
     * Setter for changing the name of the album
     *
     * @param newName The name to change to
     */
    public void changeName(String newName) {
        name = newName;
    }

    /**
     * Clones the album. Contains deep clone logic.
     *
     * @return Cloned object of the current album.
     * @throws CloneNotSupportedException clone exception.
     */
    public Object clone() throws CloneNotSupportedException {

        Album clone = (Album) super.clone();
        //  Making a deep copy of photoList
        List<Photo> clonedPhotoList = new ArrayList<>();
        for (Photo currPhoto : photoList) {
            clonedPhotoList.add(currPhoto);
        }
        clone.photoList = clonedPhotoList;
        return clone;
    }
}

public class Albums extends AppCompatActivity {
    public static final int ADD_ALBUM_CODE = 1;
    public static final int EDIT_ALBUM_CODE = 2;
    public static final int REMOVE_ALBUM_CODE = 3;
    private ListView listView;
    private ArrayList<Album>albums;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_list);
        Toolbar toolbar=(Toolbar)findViewById(R.id.my_toolbar);

        listView=(ListView) findViewById(R.id.albums_list);
        listView.setAdapter(new ArrayAdapter<Album>(this,R.layout.album,albums));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>parent, View view, int position, long id){
                showAlbum(position);
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void addAlbum() {
        Intent intent=new Intent (this, AddEditPhoto.class);
        startActivityForResult(intent,ADD_ALBUM_CODE);
    }
    public void showAlbum(int pos){
        Bundle bundle=new Bundle();
        Album album=albums.get(pos);
        ListView photolistView=(ListView)findViewById(R.id.photos_list);
        photolistView.setAdapter(new PhotoAdapter(Albums.this,album.getPhotoList()));

        Intent intent=new Intent(this,Photos.class);
        intent.putExtras(bundle);
        startActivityForResult(intent,EDIT_ALBUM_CODE);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Albums Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
