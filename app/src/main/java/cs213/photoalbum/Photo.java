package cs213.photoalbum;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 12/15/16.
 */
public class Photo implements Serializable, Cloneable {
    private File file;
    private String filename;
    private List<Tag> tagList;


    public Photo(File file, String filename) {
        this.file = file;
        this.filename = filename;
        this.tagList = new ArrayList<>();
    }

    /**
     * Getter for the photo path.
     *
     * @return String
     */
    public String getPath() {
        return file.toURI().toString();
    }

    /**
     * Getter for the File type associated with the photo
     *
     * @return File
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Getter for the file name.
     *
     * @param withExtention Boolean flag for adding extension
     * @return String file name
     */
    public String getFileName(boolean withExtention) {
        String[] args = file.toURI().toString().split("/");
        if (withExtention) {
            return args[args.length - 1];
        }
        return args[args.length - 1].split(".")[0];
    }

    /**
     * Getter for the caption associated of the current photo caption
     *
     * @return
     */
    public String getCaption() {
        return filename;
    }


    /**
     * Logic check to see if current photo contains given tag.
     *
     * @param tag tag to check with the current tag list.
     * @return boolean value
     */
    public boolean containsTag(Tag tag) {
        for (Tag currTag : tagList) {
            if (tag.equals(currTag)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Setter to add tag into list of tags
     *
     * @param type  given tag type
     * @param value given tag value
     * @return Reference of tag that was added.
     */
    public Tag addTag(String type, String value) {
        Tag newTag = new Tag(type, value);
        if (containsTag(newTag)) {
            return null;
        }
        tagList.add((newTag));
        return newTag;
    }

    /**
     * Getter for the list of tags currently associated with the photo.
     *
     * @return List of tags.
     */
    public List<Tag> getTagList() {
        return tagList;
    }

    /**
     * Set the new caption
     *
     * @param newFileName new caption
     */
    public void changeFileName(String newFileName) {
        filename = newFileName;
    }

    /**
     * Equals method.
     *
     * @param otherPhoto the other photo
     * @return boolean value
     */
    public boolean equals(Photo otherPhoto) {
        if (this == otherPhoto) {
            return true;
        }
        return this.file.equals(otherPhoto.file);
    }

    public int compareTo(Photo otherPhoto) {
        if (this == otherPhoto) {
            return 0;
        }
        if (this.filename.compareTo(otherPhoto.filename) > 0) {
            return 1;
        } else if (this.filename.compareTo(otherPhoto.filename) < 0) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Clone
     * <p>
     * Contains deep clone for the list of tags associated with the tag.
     *
     * @return
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        Photo clone = (Photo) super.clone();
        List<Tag> clonedTags = new ArrayList<>();
        clonedTags.addAll(tagList);
        clone.tagList = clonedTags;
        clone.file = new File(file.getPath());
        //clone.calendar = (Calendar)calendar.clone();
        return clone;
    }
}
