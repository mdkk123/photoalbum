package cs213.photoalbum;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
//Add Edit Photos

public class AddEditPhoto extends AppCompatActivity {
    public static final String PHOTO = "photo";
    public static final String PHOTO_INDEX = "photoIndex";
    public static final String PHOTO_NAME = "photoName";
    public static final String PHOTO_TAG = "photoTag";
    public static final int ADD_TAG_CODE=1;
    public static final int SHOW_TAG_CODE=2;
    public static final int REMOVE_TAG_CODE=3;

    private ImageView image;
    private int photoIndex;
    private EditText photoName;
    private ArrayList<Tag> photoTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_photo);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        //get the fields

        image = (ImageView) findViewById(R.id.imageView);
        photoName = (EditText) findViewById(R.id.photos_name);

        //populate the listView with photoTag
        ListView tagList = (ListView) findViewById(R.id.tag_list);
        tagList.setAdapter(new ArrayAdapter<Tag>(this, R.layout.tag, photoTag));
        tagList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showTag(position);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.photo_item_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }


    public void showTag(int pos){
        Bundle bundle=new Bundle();
        Tag tag=photoTag.get(pos);
        bundle.putInt(TagDetail.TAG_INDEX,pos);
        bundle.putString(TagDetail.TAG_TYPE,tag.getType());
        bundle.putString(TagDetail.TAG_VALUE,tag.getValue());
        Intent intent=new Intent(this,TagDetail.class);
        startActivityForResult(intent,SHOW_TAG_CODE);
    }
    // called when the user taps the Cancel button
    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void save(View view) {
        String name = photoName.getText().toString();
        ListView tagList = (ListView) findViewById(R.id.tag_list);
        tagList.setAdapter(new ArrayAdapter<Tag>(this, R.layout.tag, photoTag));
        ArrayList<Tag> tags = new ArrayList<Tag>();
        int count = tagList.getCount();
    }
}
