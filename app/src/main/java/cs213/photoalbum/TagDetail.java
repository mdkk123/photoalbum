package cs213.photoalbum;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by admin on 12/14/16.
 */

public class TagDetail extends AppCompatActivity {
    public static final String TAG_TYPE="tagtype";
    public static final String TAG_VALUE="tagvalue";
    public static final String TAG_INDEX="tagindex";
    public static final int ADD_TAG_CODE=1;
    private TextView tagtype, tagvalue;
    private int tagindex;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        //get the fields
        tagtype=(TextView)findViewById(R.id.tag_type);
        tagvalue=(TextView) findViewById(R.id.tag_value);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            tagindex=bundle.getInt(TAG_INDEX);
            tagtype.setText(bundle.getString(TAG_TYPE));
            tagvalue.setText(bundle.getString(TAG_VALUE));
        }

    }

    // called when the user taps the Back button
    public void back(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }
    //called when the user taps the Delete button
    public void delete(View view){
        Bundle bundle=new Bundle();


    }
}
