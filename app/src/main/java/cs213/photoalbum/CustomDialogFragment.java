package cs213.photoalbum;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by admin on 12/14/16.
 */

public class CustomDialogFragment extends DialogFragment {
    public static final String MESSAGE_KEY="message_key";
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        //use Builder class for convenient dialog construction
        Bundle bundle=getArguments();
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setMessage(bundle.getString(MESSAGE_KEY))
                .setPositiveButton("OK",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        //nothing to do
                    }
                });
        //create AlertDialog and return
        return builder.create();
    }
}
