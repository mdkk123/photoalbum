package cs213.photoalbum;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class will define the photo model used across the application.
 *
 *
 * @created 11/18/2016
 */

    public class Photos extends AppCompatActivity {
        public static final int ADD_PHOTO_CODE = 1;
        public static final int EDIT_PHOTO_CODE = 2;
        public static final int REMOVE_PHOTO_CODE=3;
        private ListView listView;
        private ArrayList<Photo> photos;

        /**
         * ATTENTION: This was auto-generated to implement the App Indexing API.
         * See https://g.co/AppIndexing/AndroidStudio for more information.
         */
        private GoogleApiClient client;

            //set view as album (photo list)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.album);
            listView=(ListView) findViewById(R.id.photos_list);
            listView.setAdapter(new ArrayAdapter<Photo>(this,R.layout.photo,photos));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?>parent, View view, int position, long id){
                    showPhoto(position);
                }
            });

            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        }

        /**
         * ATTENTION: This was auto-generated to implement the App Indexing API.
         * See https://g.co/AppIndexing/AndroidStudio for more information.
         */

        public void addPhoto() {
            Intent intent=new Intent (this, AddEditPhoto.class);
            startActivityForResult(intent,ADD_PHOTO_CODE);
        }
        public void showPhoto(int pos){
            Bundle bundle=new Bundle();
            Photo photo=photos.get(pos);
            bundle.putInt(AddEditPhoto.PHOTO_INDEX,pos);
            ListView tagListView=(ListView)findViewById(R.id.tag_list);
            tagListView.setAdapter(new TagAdapter(Photos.this,photo.getTagList()));
            bundle.putString(AddEditPhoto.PHOTO_NAME,photo.getCaption());
            Intent intent=new Intent(this,AddEditPhoto.class);
            intent.putExtras(bundle);
            startActivityForResult(intent,EDIT_PHOTO_CODE);
        }




        public Action getIndexApiAction() {
            Thing object = new Thing.Builder()
                    .setName("Photos Page") // TODO: Define a title for the content shown.
                    // TODO: Make sure this auto-generated URL is correct.
                    .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                    .build();
            return new Action.Builder(Action.TYPE_VIEW)
                    .setObject(object)
                    .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                    .build();
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.photo_item_menu, menu);
            return super.onCreateOptionsMenu(menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_add:
                    addPhoto();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }

        @Override
        public void onStart() {
            super.onStart();

            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            client.connect();
            AppIndex.AppIndexApi.start(client, getIndexApiAction());
        }

        @Override
        public void onStop() {
            super.onStop();

            // ATTENTION: This was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            AppIndex.AppIndexApi.end(client, getIndexApiAction());
            client.disconnect();
        }
    }

